﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using WebComics.Model;
using System.ComponentModel;
using UIKit;
using Foundation;
using System.Threading.Tasks;


namespace WebComics
{
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
		void Onback(object sender, System.EventArgs e)
        {
			Navigation.PopModalAsync();
        }
    }
}
