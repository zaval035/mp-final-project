﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using WebComics.Model;
using System.Collections.ObjectModel;
using WebComics.Models;

namespace WebComics
{
    public partial class MoreStudentInfo : ContentView
    {
		string LinkPage;
        public MoreStudentInfo()
        {
       
            InitializeComponent();
        }

        public MoreStudentInfo(CellItem CellItem)
        {
            InitializeComponent();

            BindingContext = CellItem;

            LinkPage = CellItem.url;
        }
        void HandleOnClick(object sender, System.EventArgs e)
        {
            var uri = new Uri(LinkPage);
            Device.OpenUri(uri);
        }

		public static explicit operator Page(MoreStudentInfo v)
		{
			throw new NotImplementedException();
		}
	}
}