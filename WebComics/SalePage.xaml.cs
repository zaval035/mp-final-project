﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using WebComics.Model;
using System.ComponentModel;
using UIKit;
using Foundation;
using System.Threading.Tasks;

namespace WebComics
    {
        public partial class SalePage : ContentPage
        {
            public SalePage()
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(SalePage)}:  ctor");
                InitializeComponent();
                
            }

	 void Handle_about(object sender, System.EventArgs e)
        {

			Navigation.PushModalAsync(new AboutPage());
        } 

            async void OnOption_2(object sender, System.EventArgs e)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnOption_2)}");

                string response = await DisplayActionSheet("Share with Friends",
                                  "Cancel",
                                  null,
                                  "Facebook",
                                  "Twitter");
                Debug.WriteLine($"User picked:  {response}");

                if (response.Equals("Facebook", StringComparison.OrdinalIgnoreCase))
                {
				Device.OpenUri(new Uri("http://facebook.com"));
                }
    			else if (response.Equals("Twitter", StringComparison.OrdinalIgnoreCase))
                {
                    Device.OpenUri(new Uri("http://twitter.com"));
                }
          
            }

            }
        }
    
