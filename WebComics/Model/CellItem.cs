﻿using System;
using Xamarin.Forms;

namespace WebComics.Models
{
    public class CellItem
    {

        public string webName
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }

        public ImageSource ImageIcon
        {
            get;
            set;
        }
        public string MoreInfo
        {
            get;
            set;
        }
    }
}

