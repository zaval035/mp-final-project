﻿using System;
using System.ComponentModel;

namespace WebComics.Model
{
    public class TabbedPage1ViewModel: INotifyPropertyChanged
    {
        private int counterValue = 1;
        public int CounterValue
        {
            get { return this.counterValue; }
            set { 
                if (counterValue != value)
                {
                    counterValue = value;
                    OnPropertyChanged("CounterValue");
                }
            }
        }

        
        public TabbedPage1ViewModel()
        {
            
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
