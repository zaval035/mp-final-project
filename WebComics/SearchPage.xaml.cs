﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using WebComics.Model;
using System.Collections.ObjectModel;

using WebComics.Models;

namespace WebComics
{
    public partial class SearchPage : ContentPage
    {
        public SearchPage()
        {
            InitializeComponent();
            CellsListView.ItemTapped += RecognizerTapped;
            ShowList();


        }

		void Handle_about(object sender, System.EventArgs e)
        {
            
            Navigation.PushModalAsync(new AboutPage());

        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            ShowList();
            CellsListView.IsRefreshing = false;
        }

        void HandleDeleteClicked(object sender, System.EventArgs e)
        {
            var newSender = ((MenuItem)sender).BindingContext;
            var selectedItem = (CellItem)newSender;
            var newList = new ObservableCollection<CellItem>() { };
            foreach (var item in CellsListView.ItemsSource)
            {
                var cellItem = (CellItem)item;
                if (cellItem.webName != selectedItem.webName)
                {
                    newList.Add(cellItem);
                }
            }
            CellsListView.ItemsSource = newList;

        }

        void HandleMoreClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var CellItem = (CellItem)menuItem.CommandParameter;
            Navigation.PushModalAsync((Xamarin.Forms.Page)new MoreStudentInfo(CellItem));
        }




        private void ShowList()
        {
            var forListView = new ObservableCollection<CellItem>()
            {
                new CellItem()
                {
                    ImageIcon = ImageSource.FromFile("1.png"),
                    webName="1. XKCD.com",
                    Description="Website",
					url="https://xkcd.com/",
                    MoreInfo="See more info"
                },

                new CellItem()
                {
                    ImageIcon = ImageSource.FromFile("1.png"),
                    webName = "2. ReadComic Online",
                    Description="Website",
					url="http://readcomiconline.to/",
                    MoreInfo="See more info"
                },

                 new CellItem()
                {
                    ImageIcon = ImageSource.FromFile("1.png"),
                    webName = "3. Pixton",
                    Description="Website",
					url="https://www.pixton.com/",
                        MoreInfo="See more info"
                },

                 new CellItem()
                {
                    ImageIcon = ImageSource.FromFile("1.png"),
                    webName = "4. Daily Comics",
                    Description="Website",
					url="http://comics.azcentral.com/",
                        MoreInfo="See more info"
                },

                 new CellItem()
                {
                    ImageIcon = ImageSource.FromFile("1.png"),
                    webName = "5. Create your own comic",
                    Description="Website",
					url="https://www.canva.com/create/comic-strips/",
                        MoreInfo="See more info"
                },


            };

            CellsListView.ItemsSource = forListView;
        }

        void RecognizerTapped(object sender, ItemTappedEventArgs e)
        {
            var tappedEvent = e;
            var websiteItem = (CellItem)tappedEvent.Item;
            Device.OpenUri(new Uri(websiteItem.url));
        }
        void NavigateToUrl(object sender, System.EventArgs e)
        {
            var listViewItem = (MenuItem)sender;
            var url = (string)listViewItem.CommandParameter;
            Device.OpenUri(new Uri(url));
        }
    }
}