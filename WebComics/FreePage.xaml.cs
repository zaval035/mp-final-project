﻿using Plugin.Settings;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using WebComics.Model;
using System.Threading.Tasks;

namespace WebComics
{
    public partial class FreePage : ContentPage
    {
        public TabbedPage1ViewModel globalCounterByPage =  new TabbedPage1ViewModel();
        public FreePage()
        {
            InitializeComponent();
            BindingContext = globalCounterByPage;

        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            SearchPage_async();
            Debug.WriteLine($"**** {this.GetType().Name} .{nameof(OnAppearing)} ");
        }

        void Handle_nextPage(object sender, System.EventArgs e)
        {
            globalCounterByPage.CounterValue++;
            SearchPage_async();
        }
        void Handle_backPage(object sender, System.EventArgs e)
        {
            globalCounterByPage.CounterValue--;
            SearchPage_async();
        }

	 void Handle_about(object sender, System.EventArgs e)
        {
			

			Navigation.PushAsync(new AboutPage());
                    
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name} .{nameof(OnDisappearing)} ");
        }


         async void SearchPage_async()
        {
            if (CrossConnectivity.Current.IsConnected == true)
            {
                HttpClient client = new HttpClient();
                string resultFinal = "";
                var uri = new Uri(
                    string.Format(
                        $"https://xkcd.com/" + globalCounterByPage.CounterValue + "/info.0.json"));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request);
                WebComicsApp dictionayApiData = null;
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    dictionayApiData = JsonConvert.DeserializeObject<WebComicsApp>(content);


                    resultFinal += $"\n Title: { dictionayApiData.safe_title}";
                    resultFinal += $"\n\n Description: { dictionayApiData.transcript}";
                    /*resultFinal += $"\n\n Month: { dictionayApiData.month}";
                    resultFinal += $"\n Num: { dictionayApiData.num}";
                    resultFinal += $"\n Link: { dictionayApiData.link}";
                    resultFinal += $"\n\n Year: { dictionayApiData.year}";
                    resultFinal += $"\n News: { dictionayApiData.news}";
                    resultFinal += $"\n Safe_Title: { dictionayApiData.safe_title}";
                    resultFinal += $"\n\n Transcript: { dictionayApiData.transcript}";
                    resultFinal += $"\n Alt: { dictionayApiData.alt}";
                    resultFinal += $"\n Img: { dictionayApiData.img}";
                    resultFinal += $"\n\n Title: { dictionayApiData.title}";
                    resultFinal += $"\n Day: { dictionayApiData.day}";*/


                    Result.Text = resultFinal.Substring(0, resultFinal.Length);
                    comicImage.Source = dictionayApiData.img;
                }

            }
            else
            {
                await DisplayAlert("Alert", "Not Connected to Internet", "OK");
            }
        }

    }
}
